git config --global user.email "travis@travis-ci.org"
git config --global user.name "travis-ci"
git clone --quiet --branch=gh-pages https://${GH_TOKEN}@github.com/kaashif/quenya-verb.git gh-pages
cabal configure
cabal haddock --internal
rm -rf gh-pages/*
cp -r dist/doc/html/quenya-verb/* gh-pages/
cd gh-pages
git add -f .
git commit -m "Latest Haddock on travis build $TRAVIS_BUILD_NUMBER"
git push -fq origin gh-pages


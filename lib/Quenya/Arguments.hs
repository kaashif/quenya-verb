{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings #-}
module Quenya.Arguments where
import System.Environment (getEnvironment)
import System.Directory (setCurrentDirectory)
import System.Console.CmdArgs
import Paths_quenya_verb
    
-- | Arguments structure for cmdArgs
data ServerArgs = ServerArgs { address :: String
                             , port :: String
                             , cwd :: String
                             , logging :: Bool
                             } deriving (Show, Data, Typeable)

-- | Arguments + annotations for cmdArgs
serverArgs :: IO ServerArgs
serverArgs = do
  d <- getDataFileName ""
  return $ ServerArgs { address = "127.0.0.1" &= help "Address to bind to" &= typ "ADDRESS"
                      , port = "8080" &= help "Port to bind to" &= typ "PORT"
                      , cwd = d &= help "Directory to chdir to before starting server"  &= typ "DIR"
                      , logging = False &= help "Enable logging"
                      } &= summary "Quenya verb conjugator server v0.0.1"
                        &= program "quenya-verb-server"
                        &= details ["Report bugs to kaashif@kaashif.co.uk"]

type Port = Int
type Address = String
    
-- | Returns a few arguments (handled by the web app) and handles the rest itself
getArgs :: IO (Port, Address, Bool)
getArgs = do
  a <- serverArgs >>= cmdArgs
  setCurrentDirectory $ cwd a
  return ((read $ port a) :: Int, address a, logging a)

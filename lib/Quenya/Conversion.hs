module Quenya.Conversion where
import Data.Maybe
import qualified Data.Map as M
import Quenya.Conjugator (Tense(..), Person(..), Stem(..))

-- | Maps human-readable tenses to 'Tense's
tenses :: M.Map String Tense
tenses = M.fromList [ ("present", Present)
                    , ("aorist", Aorist)
                    , ("past", Past)
                    , ("perfect", Perfect)
                    , ("future", Future)
                    ]

-- | Maps human-readable subject pronouns to 'Person's
subjects :: M.Map String Person
subjects = M.fromList [ ("I", FstSg)
                      , ("you", SndFamiliarSg)
                      , ("he/she", ThdAnimateSg)
                      , ("it", ThdInanimateSg)
                      , ("we inclusive", FstInclusivePl)
                      , ("we exclusive", FstExclusivePl)
                      , ("you plural", SndFamiliarPl)
                      , ("they", ThdAnimatePl)
                      , ("none", NoOne)
                      ]

-- | Maps human-readable object pronouns to 'Person's
objects :: M.Map String Person
objects = M.fromList [ ("me", FstSg)
                     , ("you", SndFamiliarSg) 
                     , ("him/her", ThdAnimateSg)
                     , ("it", ThdInanimateSg)
                     , ("us inclusive", FstInclusivePl)
                     , ("us exclusive", FstExclusivePl)
                     , ("you plural", SndFamiliarPl)
                     , ("they", ThdAnimatePl)
                     , ("none", NoOne)
                     ]

-- | Consolidated human-readable to 'Person' map
people :: M.Map String Person
people = M.union subjects objects

-- | Looks up what a human-readable tense name is
makeTense :: String -> Maybe Tense
makeTense s = M.lookup s tenses

-- | Looks up what a human-readable pronoun is
makePerson :: String -> Maybe Person
makePerson s = M.lookup s people

-- | Decides what type of stem a stem is
makeStem :: String -> Stem
makeStem s
    | s == "" = PrimitiveStem ""
    | last s == 'a' = AStem s
    | last s == 'u' = UStem s
    | otherwise = PrimitiveStem s

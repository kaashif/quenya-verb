Quenya Verb Conjugator 
======================
![Build status](https://travis-ci.org/kaashif/quenya-verb.svg?branch=master)
![Hackage](https://img.shields.io/hackage/v/quenya-verb.svg)

I've seen lots of Quenya dictionaries on the internet (a great one is
<http://elfdict.com>), but I haven't seen a Quenya verb conjugator,
for people who are far too lazy to actually look up how to conjugate a
verb and just want it done.

This program, when run, starts a Warp server, with a RESTful API for
conjugating verbs (and adding verbs, in the future).

Still to come
-------------
- More special cases for irregular verbs
- Fixing some cases where "ë" appears where it should just be "e"

Building
--------
```bash
$ git clone git://github.com/kaashif/quenya-verb.git
$ cd quenya-verb
$ cabal sandbox init
$ cabal install
```

Or, from Hackage:

```bash
$ cabal install quenya-verb
```
Running
-------
```bash
$ quenya-verb-server --help
Quenya verb conjugator server v0.0.1

quenya-verb-server [OPTIONS]

Common flags:
  -a --address=ADDRESS  Address to bind to
  -p --port=PORT        Port to bind to
  -c --cwd=DIR          Directory to chdir to before starting server
  -l --logging          Enable logging
  -? --help             Display help message
  -V --version          Print version information
     --numeric-version  Print just the version number

Report bugs to kaashif@kaashif.co.uk
```

If run with no arguments, the server will chdir to the cabal data
directory (where the default index.html is), and will run on
<http://localhost:8080>.

API
---
It's very simple, just send a POST request to `/api/verb` with the
following in the body:

```json
{
	"stem":"ahya",
	"tense":"aorist",
	"subject":"me",
	"object":"you"
}
```

And you'll get a response like the following:

```json
{
	"conjugated_verb":"ahyanyet"
}
```

Contact
-------
Feel free to shoot me an email at <kaashif@kaashif.co.uk> if you have
any queries or want to contribute. Alternatively, submit an issue or
pull request.

Copyright Notice
----------------
Copyright (C) 2015 Kaashif Hymabaccus

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
